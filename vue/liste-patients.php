<?php
include("../controlleur/controlleur-patient.php");
include("../controlleur/controlleur-rendezvous.php");
include("header.php")
?>

<div class="container">
  <div class="row">
    <form class="form-inline my-2 my-lg-0 form-search">
      <input class="form-control mr-sm-2" type="search" name="q" placeholder="Recherche par nom..." aria-label="Recherche par nom...">
    </form>
    <table class="table table-striped">
      <h2>Visualisation des patient</h2>
        <div style="width: 10%; border: 1px solid black; margin:auto;"></div>
        <tr><th>Id</th><th>Nom</th><th>Prénom</th><th>Action sur le patient</th></tr>
<?php
  while ($patients = $reqPatients->fetch()) {
?>
    <tr>
      <td><?php echo $patients['id'] ?></td>
      <td><?php echo $patients['lastname'] ?></td>
      <td><?php echo $patients['firstname'] ?></td>
      <td>
        <a href="profil-patient.php?action=edit&id=<?= $patients['id'] ?>" class="btn btn-primary"><span title="Supprimer le patient" class="glyphicon glyphicon-user"></span></a>
        <a href="?action=delRendezvousPatient&id=<?= $patients['id'] ?>" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></a>
      </td>
    </tr>

<?php
  }
?>
  </table>
   
    <ul class="pagination">
      <li class="<?php if($current == '1') { echo "disabled"; } ?>"><a href="?p=<?php if($current != '1') { echo $current-1; }else{echo $current;} ?>">&laquo;</a></li>
        <?php
        for ($i=1; $i<= $nbPage ; $i++) { 
          if ($i == $current) {
            ?>
              <li class="active"><a href="?page=<?php echo $i ?>"><?php echo $i ?></a></li>
              <?php
            }else{
              ?>
              <li><a href="?p=<?php echo $i ?>"><?php echo $i ?></a></li>
              <?php
      }
    }
      ?>
            <li class="<?php if($current == $nbPage) { echo "disabled"; } ?>"><a href="?p=<?php if($current != $nbPage) { echo $current+1; }else{echo $current;} ?>">&raquo;</a></li>
    </ul>
    </div>
  </div>
  </body>
</html>