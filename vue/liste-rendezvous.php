<?php
include("../controlleur/controlleur-rendezvous.php");
include("header.php");
?>

<div class="container">
  <div class="row">
    <table class="table table-striped">
      <h2>Visualisation des rendez-vous</h2>
      <div style="width: 10%; border: 1px solid black; margin:auto;"></div>
      <tr><th>Id</th><th>Date du rendez-vous</th><th>Id du patient</th><th>Action</th></tr>

<?php
  foreach ($rendezvous as $r) {
?>
    <tr>
      <td><?php echo $r['id'] ?></td>
      <td><?php echo $r['dateHour'] ?></td>
      <td><?php echo $r['idPatients'] ?></td>
      <td>
        <a href="rendezvous.php?action=editRendezvous&id=<?= $r['id'] ?>" class="btn btn-primary"><span class=" glyphicon glyphicon-time"></span></a>
        <a href="?action=del&id=<?= $r['id'] ?>" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></a>
      </td>
    </tr>

<?php
  }
?>
  </table>
    </div>
  </div>
  </body>
</html>