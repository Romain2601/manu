<?php
include ("../controlleur/controlleur-patient.php"); 
include("header.php");
?>
  
<div class="container">
  <div class="row">
    <div class="content-wrapper">
      <section class="content-header">
        <form enctype="multipart/form-data" class="well form-horizontal col-md-offset-2 col-md-8 form-ajout" action="" method="get">
			     <fieldset>
				    <legend>Ajout d'un patient</legend>
            <div style="width: 10%; border: 1px solid black; margin:auto; margin-bottom: 20px;"></div>
            <input type="hidden" name="action" value="add" >
            <input type="text" name="lastname" class="form-control" placeholder="Nom" required>
            <input type="text" name="firstname" class="form-control" placeholder="Prénom" required>
            <input type="date" name="birthdate" class="form-control" placeholder="Date d'anniversaire" required>
            <input type="tel" pattern="^\+?\s*(\d+\s?){8,}$" name="phone" class="form-control" placeholder="Numéro de téléphone" required>
            <input type="mail" name="mail" class="form-control" placeholder="Mail" required>
            <button class="btn btn-primary btn-ajoutrnedpat">Ajouter le patient</button>
          </fieldset>
        </form>
      </section>
    </div>  
  </div>
</div>
</body>
</html>
  
