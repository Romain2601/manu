<?php
include ("../controlleur/controlleur-rendezvous.php"); 
include("header.php");
?>
<div class="container">
  <div class="row">
    <div class="content-wrapper">
      <section class="content-header">
        <form enctype="multipart/form-data" class="well form-horizontal col-md-offset-2 col-md-8 form-ajout" action=" " method="get">
          <fieldset>
            <legend>Ajout d'un rendez-vous</legend>
            <div style="width: 10%; border: 1px solid black; margin:auto; margin-bottom: 20px;"></div>
            <input type="hidden" name="action" value="add" >
            <input type="datetime-local" name="dateHour" class="form-control" placeholder="Date et heure" required>
            <input type="text" name="idPatients" class="form-control" placeholder="Id du patients" required>
            <button class="btn btn-primary btn-ajoutrnedpat">Ajouter le rendez-vous</button>
          </fieldset>
        </form>
      </section>  
    </div>
  </div>
</div>
</body>
</html>
  