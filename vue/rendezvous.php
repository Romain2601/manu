<?php
include("../controlleur/controlleur-rendezvous.php");
include("header.php");
?>

<div class="container">
  <div class="row">
    <table class="table table-striped">
      <h2>Rendez-vous</h2>
      <div style="width: 10%; border: 1px solid black; margin:auto;"></div>
      <tr><th>Id</th><th>Date du rendez-vous</th><th>Id du patient</th><th>Action</th></tr>

    <tr>
      <td><?php echo $rendezVous['id'] ?></td>
      <td><?php echo $rendezVous['dateHour'] ?></td>
      <td><?php echo $rendezVous['idPatients'] ?></td>
      <td>
        <a href="?action=editRendezvous&id=<?= $rendezVous['id'] ?>" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></a>
        <a href="?action=del&id=<?= $rendezvous['id'] ?>" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></a>
      </td>
    </tr>
    </table>
    <div class="content-wrapper">
      <section class="content-header">
        <form enctype="multipart/form-data" class="well form-horizontal col-md-offset-2 col-md-8 form-ajout" action="" method="get">
        <fieldset>
          <legend>Modification du rendez-vous</legend>
          <input type="hidden" name="action" value="updateRendezvous">
          <input type="hidden" name="id" value="<?php echo $rendezVous['id'] ?>">
          <input type="datetime-local" name="dateHour" class="form-control" placeholder="Date du rendez-vous" value="<?php echo $rendezVous['dateHour'] ?>">
          <button class="btn btn-primary btn-ajoutrnedpat">Modifier le rendez-vous</button>
        </fieldset>
        </form>
      </section>
    </div>
  </div>
</body>
</html>
