<?php
include("../controlleur/controlleur-patient.php");
include("../controlleur/controlleur-rendezvous.php");
include("header.php");
?>

<div class="container">
    <div class="row">
 <table class="table table-striped">
  <h2>Profil du patient</h2>
  <div style="width: 10%; border: 1px solid black; margin:auto;"></div>
  <tr><th>Id</th><th>Lastname</th><th>Firstname</th><th>Birthdate</th><th>Phone</th><th>Mail</th><th>Action</th></tr>

    <tr>
      <td><?php echo $patient['id'] ?></td>
      <td><?php echo $patient['lastname'] ?></td>
      <td><?php echo $patient['firstname'] ?></td>
      <td><?php echo $patient['birthdate'] ?></td>
      <td><?php echo $patient['phone'] ?></td>
      <td><?php echo $patient['mail'] ?></td>
      <td>
        <a href="?action=edit&id=<?= $patient['id'] ?>" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></a>
        <a href="?action=delPatients&id=<?= $patient['id'] ?>" class="btn btn-danger"><span class="glyphicon glyphicon-minus-sign"></span></a>
      </td>
    </tr>
  </table>

   <table class="table table-striped">
  <tr><th>Rendez-vous</th></tr>


<?php foreach ($rendezVousPatient as $rvp) {?>
    <tr>
      <td><?php echo $rvp['dateHour'] ?></td>
    </tr>
    <?php } ?>

  </table>

    <div class="content-wrapper">
    <section class="content-header">
      <form enctype="multipart/form-data" class="well form-horizontal col-md-offset-2 col-md-8 form-ajout" action="" method="get">
       <fieldset>
          <legend>Modification du patient</legend>
          <input type="hidden" name="action" value="updatePatient">
          <input type="hidden" name="id" value="<?php echo $patient['id'] ?>">
          <input type="text" name="lastname" class="form-control" placeholder="Nom" value="<?php echo $patient['lastname'] ?>" required>
          <input type="text" name="firstname" class="form-control" placeholder="Prénom" value="<?php echo $patient['firstname'] ?>" required>
          <input type="date" name="birthdate" class="form-control" placeholder="Date d'anniversaire" value="<?php echo $patient['birthdate'] ?>" required>
          <input type="tel" pattern="^\+?\s*(\d+\s?){8,}$" name="phone" class="form-control" placeholder="Numéro de téléphone" value="<?php echo $patient['phone'] ?>" required>
          <input type="mail" name="mail" class="form-control" placeholder="Mail" value="<?php echo $patient['mail'] ?>" required>
          <button class="btn btn-primary btn-ajoutrnedpat">Modifier le patient</button>
        </fieldset>
      </form>
    </section>
  </div>
</div>
</body>
</html>
