<!DOCTYPE html>
<html lang="en">
<head>
  <title>La Manu</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="../font/css/style.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="../index.php">ACCUEIL</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Patients<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="ajout-patient.php">Ajout d'un patient</a></li>
          <li><a href="liste-patients.php">Visualisation des patients</a></li>
        </ul>
      </li>
       <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Rendez-vous<span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="ajout-rendezvous.php">Ajout d'un rendez-vous</a></li>
          <li><a href="liste-rendezvous.php">Visualisation des rendez-vous</a></li>
        </ul>
      </li>
    </ul>
  </div>
</nav>
</body>
</html>
