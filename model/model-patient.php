<?php

$pdo = NEW PDO("mysql:host=localhost;dbname=patients", "root", "");

function ajouterPatients($lastname, $firstname, $birthdate, $phone, $mail) {
	global $pdo;
	$sql = "INSERT INTO patients VALUES(NULL, ?, ?, ?, ?, ?)";
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(1, $lastname, PDO::PARAM_STR);
	$stmt->bindValue(2, $firstname, PDO::PARAM_STR);
	$stmt->bindValue(3, $birthdate, PDO::PARAM_STR);
	$stmt->bindValue(4, $phone, PDO::PARAM_STR);
	$stmt->bindValue(5, $mail, PDO::PARAM_STR);
	return $stmt->execute();
}

function supprimerPatientsRendezvous($id) {
	global $pdo;
	$sql = "DELETE FROM patients WHERE id=?";
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(1, $id, PDO::PARAM_INT);
	$stmt->execute();
	$sql = "DELETE FROM appointments WHERE idPatients=?";
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(1, $id, PDO::PARAM_INT);
	return $stmt->execute();
}

function modifierPatients($id, $lastname, $firstname, $birthdate, $phone, $mail) {
	global $pdo;
	$sql = "UPDATE patients SET lastname='$lastname', firstname='$firstname', birthdate='$birthdate', phone='$phone', mail='$mail' WHERE id='$id'";
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(1, $lastname, PDO::PARAM_STR);
	$stmt->bindValue(2, $firstname, PDO::PARAM_STR);
	$stmt->bindValue(3, $birthdate, PDO::PARAM_STR);
	$stmt->bindValue(4, $phone, PDO::PARAM_STR);
	$stmt->bindValue(5, $mail, PDO::PARAM_STR);
	$ret = $stmt->execute();
    $stmt->closeCursor();
}


function getPatient($id) {
	global $pdo;
	$sql = "SELECT * FROM patients WHERE id=?";
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(1, $id, PDO::PARAM_INT);
	$stmt->execute();

	return $stmt->fetch();
}



