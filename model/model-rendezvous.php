<?php

$pdo = NEW PDO("mysql:host=localhost;dbname=patients", "root", "");

function ajouterRendezvous($dateHour, $idPatients) {
	global $pdo;
	$sql = "INSERT INTO appointments VALUES(NULL, ?, ?)";
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(1, $dateHour, PDO::PARAM_STR);
	$stmt->bindValue(2, $idPatients, PDO::PARAM_STR);
	return $stmt->execute();
}

function supprimerRendezvous($id) {
	global $pdo;
	$sql = "DELETE FROM appointments WHERE id=?";
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(1, $id, PDO::PARAM_INT);
	return $stmt->execute();
}

function modifierRendezvous($id, $dateHour) {
	global $pdo;
	$sql = "UPDATE appointments SET dateHour=:datehour WHERE id=:id";
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(':datehour', $dateHour, PDO::PARAM_STR);
	$stmt->bindValue(':id', $id, PDO::PARAM_INT);
	$ret = $stmt->execute();
    $stmt->closeCursor();
}

function getRendezvous($id) {
	global $pdo;
	$sql = "SELECT * FROM appointments WHERE id=?";
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(1, $id, PDO::PARAM_INT);
	$stmt->execute();
	return $stmt->fetch();
}

function getRendezvousPatient($idPatients) {
	global $pdo;
	$sql = "SELECT * FROM appointments WHERE idPatients=?";
	$stmt = $pdo->prepare($sql);
	$stmt->bindValue(1, $idPatients, PDO::PARAM_INT);
	$stmt->execute();
	return $stmt->fetchAll();

}

function getAllPatients() {
	global $pdo;
	$sql = "SELECT * FROM appointments";
	$stmt = $pdo->prepare($sql);
	$stmt->execute();
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
}



