<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">
    <title>La Manu</title>
    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/navbars/">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="font/css/index.css" rel="stylesheet">
  <link href="font/css/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="font/css/animate.css" rel="stylesheet" />
  </head>
<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
  <div class="img-font">
  <div id="preloader">
    <div id="load"></div>
  </div>
  <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
      <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="vue/ajout-patient.php">Ajout patient</a></li>
          <li><a href="vue/ajout-rendezvous.php">Ajout rendez-vous</a></li>
        </ul>
      </div>
  </nav>
  <section id="intro" class="intro">
    <div class="slogan">
      <h2><span class="text_color" >Hôpital</span></h2>
    </div>
  </section>
</div>
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.easing.min.js"></script>
  <script src="js/jquery.scrollTo.js"></script>
  <script src="js/wow.min.js"></script>
  <script src="js/custom.js"></script>
  <script src="contactform/contactform.js"></script>
</body>
</html>
