-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 04 juin 2019 à 20:52
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `patients`
--

-- --------------------------------------------------------

--
-- Structure de la table `appointments`
--

DROP TABLE IF EXISTS `appointments`;
CREATE TABLE IF NOT EXISTS `appointments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateHour` datetime NOT NULL,
  `idPatients` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_appointments_id_patients` (`idPatients`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `appointments`
--

INSERT INTO `appointments` (`id`, `dateHour`, `idPatients`) VALUES
(6, '2019-06-14 17:30:00', 1),
(8, '2019-06-11 07:06:00', 2),
(9, '2019-06-07 11:15:00', 3),
(10, '2019-07-12 08:15:00', 3),
(11, '2019-05-23 12:16:00', 4),
(12, '2019-06-20 10:21:00', 6),
(13, '2019-08-14 09:00:00', 7),
(14, '2019-10-23 09:00:00', 7),
(15, '2019-06-13 11:30:00', 8),
(16, '2019-06-20 16:00:00', 9),
(17, '2019-08-02 12:30:00', 10),
(18, '2019-06-23 12:30:00', 2);

-- --------------------------------------------------------

--
-- Structure de la table `patients`
--

DROP TABLE IF EXISTS `patients`;
CREATE TABLE IF NOT EXISTS `patients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lastname` varchar(25) NOT NULL,
  `firstname` varchar(25) NOT NULL,
  `birthdate` date NOT NULL,
  `phone` varchar(25) DEFAULT NULL,
  `mail` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `patients`
--

INSERT INTO `patients` (`id`, `lastname`, `firstname`, `birthdate`, `phone`, `mail`) VALUES
(1, 'Felix', 'Romain', '2019-05-06', '0646715313', 'felix.romain@hotmail.fr'),
(2, 'Sacripant', 'Charest', '2019-05-06', '0550074201', 'SacripantCharest@teleworm.us'),
(3, 'Gaspar', 'Roy', '2019-05-06', '0103702598', 'GasparRoy@teleworm.us'),
(4, 'Esmeraude', 'Ratte', '2019-05-19', '0454288522', 'EsmeraudeRatte@teleworm.us'),
(5, 'Paige', 'Dumoulin', '2019-05-06', '0414784043', 'PaigeDumoulin@rhyta.com'),
(6, 'Aiglentina', 'Douffet', '2019-05-06', '0384986996', 'AiglentinaDouffet@dayrep.com'),
(7, 'Fletcher', 'Caouette', '2019-05-06', '0205193884', 'FletcherCaouette@dayrep.com'),
(8, 'Rabican', 'Brault', '2019-05-06', '0240142954', 'RabicanBrault@rhyta.com'),
(9, 'Favor', 'Dufresne', '2019-05-06', '0393922031', 'FavorDufresne@rhyta.com'),
(10, 'Tabor', 'Coupart', '2019-05-06', '0432341015', 'TaborCoupart@rhyta.com');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `FK_appointments_id_patients` FOREIGN KEY (`idPatients`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
