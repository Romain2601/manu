<?php

require_once "../model/model-rendezvous.php";

$action = isset($_GET['action']) ? $_GET['action'] : '';

$rendezVous = ['id' => '', 'dateHour' => '', 'idPatients' => ''];
//$rendezVousPatient = ['id' => '', 'dateHour' => '', 'idPatients' => ''];

switch($action)
{
    case 'add':
        $dateHour = $_GET['dateHour'];
        $idPatients = $_GET['idPatients'];
        ajouterRendezvous($dateHour, $idPatients);
        break;
    case 'del':
        $id = $_GET['id'];
        supprimerRendezvous($id);
        break;
    case 'editRendezvous':
        $id = $_GET['id'];
        $rendezVous = getRendezvous($id);
        break;
    case 'updateRendezvous':
        $id =$_GET['id'];
        $dateHour =$_GET['dateHour'];
        modifierRendezvous($id, $dateHour);
        $rendezVous = getRendezvous($id);
        break;
    case 'edit':
        $idPatients = $_GET['id'];
        $rendezVousPatient = getRendezvousPatient($idPatients);
        break;
    }

$rendezvous = getAllPatients(); 

?>