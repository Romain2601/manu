<?php

require_once "../model/model-patient.php";
require_once "../model/model-rendezvous.php";

$action = isset($_GET['action']) ? $_GET['action'] : '';

$patient = ['id' => '', 'lastname' => '', 'firstname' => '', 'birthdate' => '', 'phone' => '', 'mail' => ''];

switch($action)
{
	case 'add':
		$lastname = $_GET['lastname'];
		$firstname = $_GET['firstname'];
		$birthdate = $_GET['birthdate'];
		$phone = $_GET['phone'];
		$mail = $_GET['mail'];
		ajouterPatients($lastname, $firstname, $birthdate, $phone, $mail);
		break;
	case 'edit':
		$id = $_GET['id'];
		$patient = getPatient($id);
		break;
	case 'updatePatient':
		$id =$_GET['id'];
		$lastname =$_GET['lastname'];
		$firstname =$_GET['firstname'];
		$birthdate =$_GET['birthdate'];
		$phone =$_GET['phone'];
		$mail =$_GET['mail'];
		modifierPatients($id, $lastname, $firstname, $birthdate, $phone, $mail);
		$patient = getPatient($id);
		$rendezVousPatient = getRendezvousPatient($id);
		break;
	case 'delRendezvousPatient':
		$id = $_GET['id'];
		supprimerPatientsRendezvous($id);
		break;
	}


?>

<?php
$perPage = 3;
$req = $pdo->query('SELECT COUNT(*) AS total FROM patients');
$result = $req->fetch();
$total = $result['total'];
$nbPage = ceil($total/$perPage);
if(isset($_GET['p']) && !empty($_GET['p']) && ctype_digit($_GET['p']) == 1) {
  if($_GET['p'] > $nbPage) {
    $current = $nbPage;
  }else {
    $current = $_GET['p'];
  }
}else {
  $current = 1;
}
$firtOfPage = ($current-1)*$perPage;
$reqPatients = $pdo->query("SELECT * FROM patients ORDER BY id ASC LIMIT $firtOfPage, $perPage");
?>

<?php
if (isset($_GET['q']) AND  !empty($_GET['q'])) {
	$q = htmlspecialchars($_GET['q']);
	$reqPatients = $pdo->query('SELECT * FROM patients WHERE lastname LIKE "%'.$q.'%" ORDER BY id DESC');
}